//
//  CustomCell.swift
//  RickAndMortyApi
//
//  Created by Alex Rousseau on 23/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import UIKit

class CustomCell: UICollectionViewCell {
    
    @IBOutlet weak var holderView: UIView!
    
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var CustomIV: UIImageView!
    
    var character: Character!
    
    func setupCell(_ character: Character) {
        self.character = character
        self.nameLbl.text = self.character.name
        self.CustomIV.download(self.character.image)
        holderView.layer.cornerRadius = 25
        holderView.clipsToBounds = true
    }
    
}

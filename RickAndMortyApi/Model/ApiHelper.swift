//
//  ApiHelper.swift
//  RickAndMortyApi
//
//  Created by pppcFO1 on 23/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import Foundation

typealias ApiCompletion = (_ next: String?, _ personnages: [Character]?, _ errorString: String?) -> Void

class ApiHelper {
    private let _baseUrl = "https://rickandmortyapi.com/api/"
    
    var charactersUrl: String {
        return _baseUrl + "character/"
    }
    
    func getCharacters(_ string: String, completion: ApiCompletion?) {
        if let url = URL(string: string) {
            // manage characters
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if error != nil {
                    completion?(nil, nil, error!.localizedDescription)
                }
                
                if data != nil {
                    // convert to JSON
                    do {
                        let answerJSON = try JSONDecoder().decode(ApiResult.self, from: data!)
                        
                        completion?(answerJSON.info.next, answerJSON.results, nil)
                        
                    } catch {
                        completion?(nil, nil, error.localizedDescription)
                    }
                } else {
                    completion?(nil, nil, "No data available")
                }
            }.resume()
        } else {
            completion?(nil, nil, "invalid Url")
        }
    }
}

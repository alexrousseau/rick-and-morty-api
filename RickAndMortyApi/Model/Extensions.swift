//
//  Extensions.swift
//  RickAndMortyApi
//
//  Created by Alex Rousseau on 23/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func download(_ urlString: String) {
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            
            guard let imageData = data, let image = UIImage(data: imageData) else { return }
            
            DispatchQueue.main.async {
                self.image = image
            }
        }.resume()
    }
    
}

//
//  ViewController.swift
//  RickAndMortyApi
//
//  Created by pppcFO1 on 23/05/2019.
//  Copyright © 2019 pppcFO1. All rights reserved.
//

import UIKit

class CharactersController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var nextPage = ""
    var characters: [Character] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
        getCharacters()
    }
    
    func getCharacters() {
        ApiHelper().getCharacters(ApiHelper().charactersUrl) {
            (nextPage, characterList, error) in
            if nextPage != nil {
                print(nextPage!)
                self.nextPage = nextPage!
            }
            
            if error != nil {
                print(error!)
            }
            
            if characterList != nil {
                self.characters.append(contentsOf: characterList!)
                print(self.characters.count)
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
            
            
        }
    }


}

extension CharactersController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return characters.count
    }
    
    // Optional, especially if 1 section
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let character = characters[indexPath.item]
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCell", for: indexPath) as? CustomCell {
            cell.setupCell(character)
            return cell
        }
        
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 2 - 20
        
        return CGSize(width: width, height: width)
    }
}

